<?php include("incluir/encabezado.php") ?>
<?php include("incluir/cuerpo.php") ?>
<?php include("conexion.php") ?>

<!--Registro-->
<div class="container mp-5">

    <div class="row">

        <div class="col-md-12">
            <!--Mensaje de alerta-->
            <?php if (isset($_SESSION['message'])) { ?>
                <div class="alert alert-success d-flex align-items-center" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                        <use xlink:href="#check-circle-fill" />
                    </svg>
                    <strong>Cliente registrado exitosamente</strong>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>

        </div>
    <?php session_unset();
            } ?>
    <form action="alta.php" method="POST">
        <h1 class="display-4">Ingrese sus datos</h1>
        <div class="form-group">
            <div class="col-md-4 mb-3">
                <label for="nombre_completo">Nombre Completo</label>
                <input type="text" name="nombre_completo" class="form-control" placeholder="Ingrese su nombre y apellido" autofocus required>

            </div>
            <div class="form-group">
                <div class="col-md-4 mb-3">
                    <label for="mail">Correo Electronico</label>
                    <input type="text" name="mail" class="form-control" placeholder="Ingrese su correo electronico" required>

                </div>
                <div class="form-group">
                    <div class="col-md-4 mb-3">
                        <label for="usuario">Usuario</label>
                        <input type="text" name="usuario" class="form-control" placeholder="Ingrese su usuario" required>

                    </div>
                    <div class="form-group">
                        <div class="col-md-4 mb-3">
                            <label for="contrasena">Contrasena</label>
                            <input type="password" name="contrasena" class="form-control" placeholder="Ingrese su contrasena" required>

                        </div>
                        <input type="submit" class="btn btn-primary col-2" name="enviar_cliente" value="ENVIAR">
                        <h2></h2>
    </form>
    </div>


    <!--Tabla-->
    <div class="col md-8">
        <!--Mensaje de alerta-->
        <?php if (isset($_SESSION['message1'])) { ?>

            <div class="alert alert-danger d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                    <use xlink:href="#exclamation-triangle-fill" />
                </svg>
                <strong> Cliente eliminado </strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
    </div>
<?php session_unset();
        } ?>
<table class="table mt-5">

    <thead class="table-dark">
        <tr>
            <th> Nombre de usuario </th>
            <th> Correo electronico </th>
            <th> Usuario </th>
            <th> Acciones </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $query = "SELECT * FROM cliente";
        $resultado = mysqli_query($conexion, $query);
        while ($row = mysqli_fetch_array($resultado)) { ?>

            <tr>
                <td> <?php echo $row['nombre_completo'] ?> </td>
                <td> <?php echo $row['mail'] ?> </td>
                <td> <?php echo $row['usuario'] ?> </td>
                <td>
                    <a href="editar_clientes.php?id_cliente=<?php echo $row['id_cliente'] ?>" class="btn btn-success">
                        <i class="far fa-edit"></i>
                    </a>

                    <a href="eliminar.php?id_cliente=<?php echo $row['id_cliente'] ?>" class=" btn btn-danger">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>

</table>
</div>


</div>

</div>