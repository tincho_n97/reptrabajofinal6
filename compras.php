<?php include("incluir/encabezado.php"); ?>
<?php include("incluir/cuerpo.php"); ?>
<?php include('conexion.php') ?>

<div class="container mp-5">

    <div class="row">

        <div class="col-md-10">

            <form action="alta.php" method="POST">
                <h2 class="display-5">Realice su Compra</h2>
                <div class="container">
                    <select name="id_cliente" id="cliente" required>
                        <?php

                        $sql = $conexion->query("SELECT * FROM cliente");

                        while ($fila = $sql->fetch_array()) {
                            echo "<option value='" . $fila['id_cliente'] . "'>" . $fila['nombre_completo'] . "</option>";
                        }
                        ?>
                    </select>


                    <select name="id_producto" id="producto" required>
                        <?php

                        $sql = $conexion->query("SELECT * FROM producto");

                        while ($fila = $sql->fetch_array()) {
                            echo "<option value='" . $fila['id_producto'] . "'>" . $fila['nombre_producto'] . "</option>";
                        }
                        ?>
                    </select>

                    <button class="btn btn-primary col-2" type="submit" name="enviar_compra">Enviar</button>


                </div>

            </form>

        </div>
    </div>



    <body>

        <div class="container p-4">

            <div class="col-md-12">
                <table class="table mt-5">
                    <thead class="table-dark ">
                        <tr>
                            <th> Cliente </th>
                            <th> Producto </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $query = "SELECT * FROM compras";
                        $conexion = mysqli_connect('localhost', 'root', '', 'proyectofinal');
                        $resultado = mysqli_query($conexion, $query);

                        while ($row = mysqli_fetch_array($resultado)) {  ?>

                            <tr>
                                <td><?php echo $row['id_cliente'] ?></td>
                                <td><?php echo $row['id_producto'] ?></td>
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
                <div class="container">
                    <a class="btn btn-primary col-2" href="index.php"> Volver al inicio </a>
                </div>

            </div>
        </div>