<?php
include('conexion.php');

if (isset($_GET['id_cliente'])) {
    $id_cliente = $_GET['id_cliente'];
    $query = "SELECT * FROM cliente WHERE id_cliente = $id_cliente";
    $editar = mysqli_query($conexion, $query);

    if (mysqli_num_rows($editar) == 1) { //si el editar existe es decir ha enconctrado algo puedo mostrar al usuario los datos a actualizar,
        //num_rows cuantas filas tiene mi resultado, cuantos resultados me ha devuelto, si al menos tengo uno que coincida lo empiezo a mostrar por pantalla
        $row = mysqli_fetch_array($editar);
        $nombre_completo = $row['nombre_completo'];
        $mail = $row['mail'];
        $usuario = $row['usuario'];
    }
}

if (isset($_POST['actualizar'])) {

    $id = $_GET['id_cliente'];

    $nombre_completo = $_POST['nombre_completo'];
    $mail = $_POST['mail'];
    $usuario = $_POST['usuario'];

    $query = "UPDATE cliente set nombre_completo = '$nombre_completo', mail = '$mail', usuario = '$usuario' WHERE id_cliente = $id_cliente";
    //obtengo los valores desde el formulario y los actualizo tan solo donde el id sea igual al id que quiero actualizar
    $editar = mysqli_query($conexion, $query);

    $_SESSION['message3'] = 'Se actualizaron los datos correctamente';
    $_SESSION['message_type'] = 'success';


    header("Location: clientes.php");
}

?>
<?php include('incluir/encabezado.php') ?>


<div class="container p-4">
    <div class="row">

        <div class="col-md-4 mx-auto">
            <div class="card card-body">
                <form action="editar_clientes.php?id_cliente=<?php echo $_GET['id_cliente']; ?>" method="POST">
                    <!-- ENVIAMOS LOS DATOS A edit.php a traves de la propiedad id con php el cual obtenemos a traves del parametro -->
                    <div class="form-group">
                        <label for="nombre_completo">Nombre Completo</label>
                        <input type="text" name="nombre_completo" value="<?php echo $nombre_completo ?>" class="form-control" placeholder="Actualizar nombre completo">
                        <!--obtendra el valor desde la variable de php-->
                    </div>
                    <div class="form-group">
                        <label for="mail">Correo electronico</label>
                        <input type="text" name="mail" value="<?php echo $mail ?>" class="form-control" placeholder="Actualizar correo electronico">

                    </div>
                    <div class="form-group">
                        <label for="usuario">Usuario</label>
                        <input type="text" name="usuario" value="<?php echo $usuario ?>" class="form-control" placeholder="Actualizar Usuario">

                    </div>
                    <button class="btn btn-success" name="actualizar"> Actualizar</button>
                </form>
            </div>
        </div>

    </div>
</div>
<?php include('incluir/cuerpo.php') ?>