<?php
include('conexion.php');

if (isset($_GET['id_producto'])) {
    $id_producto = $_GET['id_producto'];
    $query = "SELECT * FROM producto WHERE id_producto = $id_producto";
    $editar = mysqli_query($conexion, $query);

    if (mysqli_num_rows($editar) == 1) { //si el el ditar existe es decir ha enconctrado algo puedo mostrar al usuario los datos a actualizar,
        //num_rows cuantas filas tiene mi resultado, cuantos resultados me ha devuelto, si al menos tengo uno que coincida lo empiezo a mostrar por pantalla
        $row = mysqli_fetch_array($editar);
        $nombre_producto = $row['nombre_producto'];
        $descripcion = $row['descripcion'];
        $cantidad = $row['cantidad'];
        $precio = $row['precio'];
    }
}

if (isset($_POST['actualizar'])) {

    $id = $_GET['id_producto'];

    $nombre_producto = $_POST['nombre_producto'];
    $descripcion = $_POST['descripcion'];
    $cantidad = $_POST['cantidad'];
    $precio = $_POST['precio'];


    $query = "UPDATE producto set nombre_producto = '$nombre_producto', descripcion = '$descripcion', cantidad = '$cantidad', precio ='$precio'  WHERE id_producto= $id_producto";
    //obtengo los valores desde el formulario y los actualizo tan solo donde el id sea igual al id que quiero actualizar
    $editar = mysqli_query($conexion, $query);

    $_SESSION['message4'] = 'Se actualizaron los datos correctamente';
    $_SESSION['message_type'] = 'success';


    header("Location: productos.php");
}

?>
<?php include('incluir/encabezado.php') ?>

<div class="container p-4">
    <div class="row">
        <div class="col-md-4 mx-auto">
            <div class="card card-body">
                <form action="editar_productos.php?id_producto=<?php echo $_GET['id_producto']; ?>" method="POST">
                    <!-- ENVIAMOS LOS DATOS A edit.php a traves de la propiedad id con php el cual obtenemos a traves del parametro -->
                    <div class="form-group">
                        <label for="nombre_producto"> Nombre </label>
                        <input type="text" name="nombre_producto" value="<?php echo $nombre_producto ?>" class="form-control" placeholder="Actualizar nombre del producto">
                        <!--obtendra el valor desde la variable de php-->
                    </div>
                    <div class="form-group">
                        <label for="descripcion"> Descripcion </label>
                        <textarea name="descripcion" rows="3" class="form-control" placeholder="Actualizar Descripcion"><?php echo $descripcion; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="cantidad">Cantidad</label>
                        <input type="number" name="cantidad" value="<?php echo $cantidad ?>" class="form-control" placeholder="Actualizar cantidad">

                    </div>
                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <input type="number" step="any" name="precio" value="<?php echo $precio ?>" class="form-control" placeholder="Actualizar precio">
                    </div>

                    <button class="btn btn-success" name="actualizar"> Actualizar</button>
                </form>
            </div>
        </div>

    </div>
</div>
<?php include('incluir/cuerpo.php') ?>