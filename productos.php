<?php include('incluir/encabezado.php') ?>
<?php include('incluir/cuerpo.php') ?>
<?php include("conexion.php"); ?>

<div class="container mp-5">

    <div class="row">
        <!--Mensaje de alerta-->
        <?php if (isset($_SESSION['message'])) { ?>
            <div class="alert alert-success d-flex align-items-center" role="alert">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use xlink:href="#check-circle-fill" />
                </svg>
                <strong> Producto registrado exitosamente</strong>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
    </div>
<?php session_unset();
        } ?>

<form action="alta.php" method="POST">
    <h2 class="display-4"> Ingrese los datos del producto</h2>
    <div class="form-group">
        <div class="col-md-4 mb-3">
            <label for="nombre_producto">Nombre del producto</label>
            <input type="text" name="nombre_producto" class="form-control" placeholder="Ingrese el nombre del producto" autofocus required>
        </div>
        <div class="col-md-4 mb-3">
            <label for="descripcion">Descripcion del producto</label>
            <textarea name="descripcion" rows="3" class="form-control" placeholder="Ingrese la descripcion del producto"></textarea>
        </div>
        <div class="col-md-4 mb-3">
            <label for="cantidad">Cantidad de productos</label>
            <input type="number" name="cantidad" class="form-control" placeholder="Ingrese la cantidad de productos" required>
        </div>
        <div class="col-md-4 mb-3">
            <label for="precio">Precio del producto</label>
            <input type="number" step="any" name="precio" class="form-control" placeholder="Ingrese el precio del producto" required>
        </div>
        <button class="btn btn-primary col-2" type="submit" name="enviar_producto">Enviar</button>
</form>
<div class="col md-8">
    <!--Mensaje de alerta-->
    <?php if (isset($_SESSION['message1'])) { ?>

        <div class="alert alert-danger d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                <use xlink:href="#exclamation-triangle-fill" />
            </svg>
            <strong> Producto eliminado </strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
</div>
<?php session_unset();
    } ?>
<table class="table mt-5">
    <thead class="table-dark">
        <tr>
            <th> Nombre del producto</th>
            <th> Descripcion del producto</th>
            <th> Cantidad de productos</th>
            <th> Precio del producto</th>
            <th> Acciones </th>

        </tr>
    </thead>
    <tbody>
        <?php
        $query = "SELECT * FROM producto";
        $resultado = mysqli_query($conexion, $query);
        while ($row = mysqli_fetch_array($resultado)) { ?>

            <tr>
                <td> <?php echo $row['nombre_producto'] ?> </td>
                <td> <?php echo $row['descripcion'] ?> </td>
                <td> <?php echo $row['cantidad'] ?> </td>
                <td> <?php echo $row['precio'] ?> </td>
                <td>
                    <a href="editar_productos.php?id_producto=<?php echo $row['id_producto'] ?>" class="btn btn-success">
                        <i class="far fa-edit"></i>
                    </a>

                    <a href="eliminar.php?id_producto=<?php echo $row['id_producto'] ?>" class=" btn btn-danger">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>

    </tbody>
<?php } ?>
</table>
</div>
</div>

</div>