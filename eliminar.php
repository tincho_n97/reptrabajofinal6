<?php

include("conexion.php");

if (isset($_GET['id_cliente'])) { //eliminar clientes

    $id_cliente = $_GET['id_cliente'];
    $query = "DELETE FROM cliente WHERE id_cliente = $id_cliente";
    $eliminar = mysqli_query($conexion, $query);
    if (!$eliminar) {
        die("no se ha podido eliminar el cliente");
    }

    $_SESSION['message1'] = 'cliente eliminado exitosamente';
    $_SESSION['message_type'] = 'danger';

    header("Location: clientes.php");
}


if (isset($_GET['id_producto'])) { //eliminar productos

    $id_producto = $_GET['id_producto'];
    $query = "DELETE FROM producto WHERE id_producto = $id_producto";
    $eliminar = mysqli_query($conexion, $query);
    if (!$eliminar) {
        die("no se ha podido eliminar el producto");
    }

    $_SESSION['message1'] = 'producto eliminado exitosamente';
    $_SESSION['message_type'] = 'danger';

    header("Location: productos.php");
}
